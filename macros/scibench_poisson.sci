// Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2010 - Neeraj Sharma and Matthias K. Gobbert

//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// This is computed with two methods :
// * the sparse backslash operator,
// * a sparse conjugate gradient algorithm.

// Sparse Gaussian Elimination

function [timesec,enorminf,h] = scibench_poisson ( N , plotgraph , verbose , __poisson_mysolver__ )
    // Solves the 2D Poisson PDE.
    //
    // Calling Sequence
    //   [timesec,enorminf,h] = scibench_poisson ( N , plotgraph , verbose , solver )
    //
    // Parameters
    //   N : a 1-by-1 matrix of doubles, integer values. The number of cells.
    //   plotgraph : a 1-by-1 matrix of booleans. Set to %t to plot the graphics.
    //   verbose : a 1-by-1 matrix of booleans. Set to %t to print messages.
    //   solver : a function, the linear equation solver.
    //   timesec : a 1-by-1 matrix of doubles, the time to perform the computation (in seconds)
    //   enorminf : a 1-by-1 matrix of doubles, the L-infinite error norm between the exact solution and the approximated solution
    //   h : a 1-by-1 matrix of doubles, the size of the step (h = 1/(N+1)).
    //
    // Description
    // We compute the numerical solution with finite
    // differences for the Poisson problem with homogeneous
    // Dirichlet boundary conditions.
    // We use Sparse Gaussian Elimination based on the sparse backslash operator.
    //
    // We consider the 2 dimensional problem Partial Differential Equation:
    //
    // <latex>
    // \begin{array}{rlll}
    // - \Delta u &=& f &\textrm{ in the domain,} \\
    //          u &=& 0 &\textrm{ on the frontier.}
    // \end{array}
    // </latex>
    //
    // where the Laplace operator is
    //
    // <latex>
    // \Delta u = \frac{\partial^2 u}{dx^2} + \frac{\partial^2 u}{dy^2}
    // </latex>
    //
    // We consider the domain 0 <= x1 <= 1, 0 <= x2 <= 1.
    // The function f is defined by
    // f(x, y) = −2pi^2 cos(2pix) sin^2(piy) − 2pi^2 sin^2(pix) cos(2piy).
    // The solution is u(x, y) = sin^2(pix) sin^2(piy).
    // We use a grid of N-by-N points.
    // We use a second order finite difference approximation of the Laplace
    // operator.
    // The solution of the problem is the solution of the linear system of
    // equations
    //
    // A u  = b,
    //
    // where A is a N^2-by-N^2 matrix.
    // In order to compute the matrix A, we perform the sum of two
    // Kronecker products.
    // The computation of A is fast and produces a sparse matrix A.
    //
    // Most of the CPU time is consumed by the resolution of the
    // system of linear equations, within the <literal>solver</literal> function.
    // This function must have header
    // <screen>
    // u = solver(N,b)
    // </screen>
    // where N is the number of cells, b is the sparse right hand side,
    // and u is the sparse solution of A*u=b.
    //
    // Examples
    // stacksize("max");
    // scf();
    // function u = mysolverBackslash(N,b)
    //     A = scibench_poissonA(N);
    //     u = A\b;
    // endfunction
    // [timesec,enorminf,h] = scibench_poisson(50, %t , %t , mysolverBackslash )
    //
    // // Use a PCG solver:
    // function u = mysolverPCG(N,b)
    //     tol = 0.000001;
    //     maxit = 9999;
    //     u = zeros(N^2,1);
    //     [u,flag,iter,res] = pcg(scibench_poissonAu,b,tol,maxit,[],[],u);
    // endfunction
    // [timesec,enorminf,h] = scibench_poisson(50, %f , %t , mysolverPCG );
    //
    // Bibliography
    // "A Comparative Evaluation Of Matlab, Octave, Freemat, And Scilab For Research And Teaching", 2010, Neeraj Sharma and Matthias K. Gobbert

    //
    // Compute the exact solution
    h = 1/(N+1);
    x = h:h:1-h;
    y = x;
    [X,Y] = ndgrid(x,y);
    F = (-2*(%pi^2))*(cos((2*%pi)*X) .*(sin(%pi*Y) .^2) + (sin(%pi*X) .^2).*cos((2*%pi)*Y));
    b = (h^2)*F(:);
    //
    // Compute the approximate solution
    tic;
    u = __poisson_mysolver__(N,b)
    timesec = toc();
    Uint = matrix(u,[N N]); // N.B.: Uint has only solutions on interior points
    // append boundary to x, y, and to U:
    x = [0,x,1];
    y = [0,y,1];
    [X,Y] = ndgrid(x,y);
    U = zeros(X);
    U(2:$-1,2:$-1) = Uint;
    if ( plotgraph ) then
        // plot numerical solution:
        subplot(1,2,1)
        mesh(X,Y,U)
        xlabel("x");
        ylabel("y");
        zlabel("u");
        xtitle("Solution")
    end
    // compute numerical error:
    Utrue = (sin(%pi*X) .^2) .*(sin(%pi*Y) .^2);
    E = U - Utrue;
    // Plot numerical error:
    if ( plotgraph ) then
        subplot(1,2,2)
        mesh(X,Y,E)
        xtitle("Numerical error")
        xlabel("x");
        ylabel("y");
        zlabel("u-u_h");
    end
    // compute L^inf norm of error and print:
    enorminf = max(abs(E(:)));
    if ( verbose ) then
        mprintf("N = %5d\n",N);
        mprintf("h = %24.16e\n",h);
        mprintf("h^2 = %24.16e\n",h^2);
        mprintf("enorminf = %24.16e\n",enorminf);
        mprintf("C = enorminf / h^2 = %24.16e\n",enorminf/(h^2));
        mprintf("wall clock time = %10.2f seconds\n",timesec);
    end
endfunction

