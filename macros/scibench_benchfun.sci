// Copyright (C) 2009 - 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [t,msg] = scibench_benchfun ( varargin )
    // Benchmarks a function and measure its performance.
    //
    // Calling Sequence
    //   [t,msg] = scibench_benchfun ( fun )
    //   [t,msg] = scibench_benchfun ( fun , name )
    //   [t,msg] = scibench_benchfun ( fun , name , kmax )
    //   [t,msg] = scibench_benchfun ( fun , name , kmax , verbose )
    //   [t,msg] = scibench_benchfun ( fun , name , kmax , verbose , outfun )
    //
    // Parameters
    //   fun : a function or a list, the function to be executed. If fun is a list, the first element is expected to be a function and the remaining elements of the list are input arguments of the function, which are appended at the end.
    //   name : a 1 x 1 matrix of strings, the name of the function to be executed (default name="").
    //   kmax : a 1 x 1 matrix of floating point integers, the number of executions, must be greater than 1 (default kmax=10).
    //   verbose : a 1x1 matrix of booleans, set to %t to display the result (default verbose = %t)
    //   outfun : a function or a list, the output function (default is an empty output function). If outfun is a list, the first element is expected to be a function and the remaining elements of the list are input arguments of the function, which are appended at the end.
    //   t : a kmax x 1 matrix of doubles, the system times, in seconds, required to execute the function
    //   msg : a 1 x 1 matrix of strings, a message summarizing the benchmark
    //
    // Description
    //   This function is designed to be used when measuring the
    //   performance of a function.
    //   It uses the timer function to measure the system time.
    //   The function is executed kmax times and the performance is
    //   gathered into the matrix t.
    //   The message summarizes the test and contains the mean,
    //   min and max of the times.
    //
    //   Any argument equal to the empty matrix [] is replaced by its default value.
    //
    //   The header of the function fun must be
    //   <programlisting>
    //     t=fun()
    //   </programlisting>
    //   where t measures the time to perform the task (in seconds).
    //
    // It might happen that the function requires additionnal arguments to be evaluated.
    // In this case, we can use the following feature.
    // The function fun can also be the list (f,a1,a2,...).
    // In this case f, the first element in the list, must have the header:
    //   <programlisting>
    //     t = f ( x , a1 , a2 , ... )
    //   </programlisting>
    // where the input arguments a1, a2, ...
    // will be automatically be appended at the end of the calling sequence.
    //
    // The output function outfun should have header
    //   <programlisting>
    //     stop = outfun ( k , t )
    //   </programlisting>
    // where k is the simulation index and t is the time required to perform the run.
    // Here, stop is a boolean, which is %t if the algorithm must stop.
    //
    // It might happen that the output function requires additionnal arguments to be evaluated.
    // In this case, we can use the following feature.
    // The function outfun can also be the list (outf,a1,a2,...).
    // In this case outf, the first element in the list, must have the header:
    //   <programlisting>
    //     y = outf ( x , a1 , a2 , ... )
    //   </programlisting>
    // where the input arguments a1, a2, ...
    // will be automatically be appended at the end of the calling sequence.
    //
    // Examples
    // //
    // // An example with 0 input argument
    // function t = pascalup_col0rhs ()
    //    // Pascal up matrix.
    //    // Column by column version
    //    tic ()
    //    n = 100
    //    c = eye(n,n)
    //    c(1,:) = ones(1,n)
    //    for i = 2:(n-1)
    //       c(2:i,i+1) = c(1:(i-1),i)+c(2:i,i)
    //    end
    //    t = toc()
    // endfunction
    // [t,msg] = scibench_benchfun ( pascalup_col0rhs );
    // [t,msg] = scibench_benchfun ( pascalup_col0rhs , "pascalup_col" );
    // //
    // // An example with 1 input argument
    // function t = pascalup_col (n)
    //   // Pascal up matrix.
    //   // Column by column version
    //   tic ()
    //   c = eye(n,n)
    //   c(1,:) = ones(1,n)
    //   for i = 2:(n-1)
    //     c(2:i,i+1) = c(1:(i-1),i)+c(2:i,i)
    //   end
    //   t = toc()
    // endfunction
    // [t,msg] = scibench_benchfun ( list(pascalup_col,100) , "pascalup_col" );
    // [t,msg] = scibench_benchfun ( list(pascalup_col,100) , "pascalup_col" , 10 );
    // [t,msg] = scibench_benchfun ( list(pascalup_col,100) , "pascalup_col" , 10, %f )
    // //
    // // Use a callback to print the timings
    // function stop = myoutfun(k,t)
    //   stop = %f
    //   mprintf("Run #%d, t=%.2f\n",k,t)
    // endfunction
    // scibench_benchfun ( list(pascalup_col,100) , "pascalup_col" , [] , [] , myoutfun );
    //
    // Authors
    //   2010 - DIGITEO - Michael Baudin


    [lhs, rhs] = argn()
    apifun_checkrhs ( "scibench_benchfun" , rhs , 1:5 )
    apifun_checklhs ( "scibench_benchfun" , lhs , 0:2 )
    //
    // Get arguments
    __benchfun_fun__ = varargin(1)
    name = argindefault ( rhs , varargin , 2 , "" )
    kmax = argindefault ( rhs , varargin , 3 , 10 )
    verbose = argindefault ( rhs , varargin , 4 , %t )
    __dynbenchfun_outfun__ = argindefault ( rhs , varargin , 5 , [] )
    //
    // Check Type
    apifun_checktype ( "scibench_benchfun" , __benchfun_fun__ , "benchfun_fun" , 1 , ["function" "list" "fptr"] )
    if ( typeof(__benchfun_fun__) == "list" ) then
        apifun_checktype ( "scibench_benchfun" , __benchfun_fun__(1) , "fun(1)" , 1 , ["function" "fptr"] )
    end
    apifun_checktype ( "scibench_benchfun" , name , "name" , 2 , "string" )
    apifun_checktype ( "scibench_benchfun" , kmax , "kmax" , 3 , "constant" )
    apifun_checktype ( "scibench_benchfun" , verbose , "verbose" , 4 , "boolean" )
    if ( __dynbenchfun_outfun__ <> [] ) then
        apifun_checktype ( "scibench_benchfun" , __dynbenchfun_outfun__ , "outfun" , 5 , ["function" "list" "fptr"] )
        if ( typeof(__dynbenchfun_outfun__) == "list" ) then
            apifun_checktype ( "scibench_benchfun" , __dynbenchfun_outfun__(1) , "outfun(1)" , 5 , "function" )
        end
    end
    //
    // Check Size
    apifun_checkscalar ( "scibench_benchfun" , name , "name" , 2 )
    apifun_checkscalar ( "scibench_benchfun" , kmax , "kmax" , 3 )
    apifun_checkscalar ( "scibench_benchfun" , verbose , "verbose" , 4 )
    //
    // Check Content
    apifun_checkgreq ( "scibench_benchfun" , kmax , "kmax" , 3 , 1 )
    // TODO : check that these are floating point integers

    //
    // Setup
    typfun = typeof(__benchfun_fun__)
    if ( typfun == "list" ) then
        __benchfun_fun__f = __benchfun_fun__(1)
    end
    if ( __dynbenchfun_outfun__ <> [] ) then
        cbktype = typeof(__dynbenchfun_outfun__)
        if ( cbktype == "list" ) then
            // List or tlist
            __dynbenchfun_outfun__f = __dynbenchfun_outfun__(1);
        end
    end
    //
    // Loop over the tests
    for k = 1 : kmax
        // Call the function
        if ( or ( typfun == ["function" "fptr"] ) ) then
            thistime = __benchfun_fun__()
        else
            thistime = __benchfun_fun__f(__benchfun_fun__(2:$))
        end
        //
        // Checkout output t
        if ( typeof(thistime) <> "constant" ) then
            error(sprintf(gettext("%s: The output argument t of measured function must be a real matrix of doubles.\n"), ..
                "scibench_dynbenchfun"))
        end
        if ( size(thistime,"*") <> 1 ) then
            error(sprintf(gettext("%s: The output argument t of measured function must be a 1-by-1 real matrix of doubles.\n"), ..
                "scibench_dynbenchfun"))
        end
        if ( thistime < 0 ) then
            error(sprintf(gettext("%s: The output argument t of measured function must be positive.\n"), ..
                "scibench_dynbenchfun"))
        end
        t(k) = thistime
        stop = %f
        if ( __dynbenchfun_outfun__ <> [] ) then
            if ( cbktype == "list" ) then
                // List or tlist
                stop = __dynbenchfun_outfun__f ( k , t(k) , __dynbenchfun_outfun__(2:$));
            elseif ( or(cbktype == ["function" "fptr"] ) ) then
                // Macro or compiled macro
                stop = __dynbenchfun_outfun__ ( k , t(k) );
            end
        end
        if ( stop ) then
            break
        end
    end
    if ( name == "" ) then
        msg = msprintf("%d iterations, mean=%f (s), min=%f (s), max=%f (s)\n",kmax,mean(t),min(t),max(t))
    else
        msg = msprintf("%s: %d iterations, mean=%f (s), min=%f (s), max=%f (s)\n",name,kmax,mean(t),min(t),max(t))
    end
    if ( verbose ) then
        mprintf("%s\n",msg)
    end
endfunction

function argin = argindefault ( rhs , vararglist , ivar , default )
    // Returns the value of the input argument #ivar.
    // If this argument was not provided, or was equal to the
    // empty matrix, returns the default value.
    if ( rhs < ivar ) then
        argin = default
    else
        if ( vararglist(ivar) <> [] ) then
            argin = vararglist(ivar)
        else
            argin = default
        end
    end
endfunction

