// Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function perftable = scibench_backslash ( varargin )
    // Benchmarks the backslash operator.
    //
    // Calling Sequence
    //   perftable = scibench_backslash ( )
    //   perftable = scibench_backslash ( verbose )
    //   perftable = scibench_backslash ( verbose , dispgraph )
    //   perftable = scibench_backslash ( verbose , dispgraph , timemin )
    //   perftable = scibench_backslash ( verbose , dispgraph , timemin , timemax )
    //   perftable = scibench_backslash ( verbose , dispgraph , timemin , timemax , nfact )
    //
    // Parameters
    //   verbose : a 1-by-1 matrix of booleans, set to %t to print messages during and at the end of the experiment (verbose = %t).
    //   dispgraph : a 1-by-1 matrix of booleans, set to %t to create a plot (default dispgraph = %f).
    //   timemin : a 1-by-1 matrix of doubles, the minimum time (in seconds) to measure the mflops (default timemin = 0.001). Must be positive.
    //   timemax : a 1-by-1 matrix of doubles, the maximum time (in seconds) to measure the mflops (default timemax = 0.1). Must be larger than timemin. The experiment ends when this time is reached.
    //   nfact : a 1-by-1 matrix of doubles, the multiplication factor for the size n of the matrix (default nfact = 1.2). Must be greater than 1. At each step of the algorithm, the size n of the matrix is updated with n = n * nfact.
    //   perftable : a m-by-3 matrix of doubles, the performances.
    //   perftable(k,1) : size of the matrix for experiment #k
    //   perftable(k,2) : wall clock time for experiment #k
    //   perftable(k,3) : number of megaflops achieved for experiment #k
    //
    // Description
    //   This function allows to measure the performance of the backslash operator when the
    //   number of unknown is equal to the number of equations.
    //   It measures the performance of the Gaussian elimination with row
    //   pivoting algorithm from Lapack.
    //   This test is often refered to as the "LINPACK" benchmark, but
    //   Scilab uses LAPACK.
    //
    //   This function may generate an error in the case where Scilab is
    //   ouf of stack memory.
    //
    //   To measure the performance we count the number of megaflops as (2/3*n^3 + 2*n^2)/t/1.e6,
    //   where n is the size of the matrix.
    //   The wall clock time is measured with the tic and toc functions.
    //
    //   The algorithm starts with a matrix size equal to n = 1.
    //   Then the matrix size is updated, by multiplying n by nfact.
    //
    //   For large matrices, the backslash test may fail, because the backslash operator
    //   wrongly switches to a least squares computation algorithm, instead of keeping
    //   on the Gaussian elimination.
    //   This is bug #7497 : http://bugzilla.scilab.org/show_bug.cgi?id=7497
    //
    // Any argument equal to the empty matrix [] is replaced by its default value.
    //
    // Examples
    // lines(0);
    // stacksize("max");
    // scf();
    // perftable = scibench_backslash ( )
    // // Do not print a message
    // scibench_backslash ( %f );
    // // Do not create the graph
    // scibench_backslash ( %t , %f );
    // // Set timemin
    // perftable = scibench_backslash ( [] , [] , 0.1 )
    // // Set timemax
    // perftable = scibench_backslash ( [] , [] , [] , 2 )
    // // Set nfact
    // perftable = scibench_backslash ( [] , [] , [] , [] , 1.5 )
    //
    // Bibliography
    // "Programming in Scilab", Michael Baudin, 2010, http://forge.scilab.org/index.php/p/docprogscilab/downloads/
    // "Linear algebra performances", http://wiki.scilab.org/Linalg_performances
    // "Benchmarks: LINPACK and MATLAB - Fame and fortune from megaflops", Cleve Moler, 1994
    //
    // Authors
    //   2010 - DIGITEO - Michael Baudin

    [lhs, rhs] = argn()
    apifun_checkrhs ( "scibench_backslash" , rhs , 0:5 )
    apifun_checklhs ( "scibench_backslash" , lhs , 0:1 )
    //
    // Get arguments
    verbose = argindefault ( rhs , varargin , 1 , %t )
    dispgraph = argindefault ( rhs , varargin , 2 , %t )
    timemin = argindefault ( rhs , varargin , 3 , 0.001 )
    timemax = argindefault ( rhs , varargin , 4 , 0.1 )
    nfact = argindefault ( rhs , varargin , 5 , 1.2 )
    //
    // Check Type
    apifun_checktype ( "scibench_backslash" , verbose , "verbose" , 1 , "boolean" )
    apifun_checktype ( "scibench_backslash" , dispgraph , "dispgraph" , 2 , "boolean" )
    apifun_checktype ( "scibench_backslash" , timemin , "timemin" , 3 , "constant" )
    apifun_checktype ( "scibench_backslash" , timemax , "timemax" , 4 , "constant" )
    apifun_checktype ( "scibench_backslash" , nfact , "nfact" , 5 , "constant" )
    //
    // Check Size
    apifun_checkscalar ( "scibench_backslash" , verbose , "verbose" , 1 )
    apifun_checkscalar ( "scibench_backslash" , dispgraph , "dispgraph" , 2 )
    apifun_checkscalar ( "scibench_backslash" , timemin , "timemin" , 3 )
    apifun_checkscalar ( "scibench_backslash" , timemax , "timemax" , 4 )
    apifun_checkscalar ( "scibench_backslash" , nfact , "nfact" , 5 )
    //
    // Check Content
    apifun_checkgreq ( "scibench_backslash" , timemin , "timemin" , 3 , 0 )
    apifun_checkgreq ( "scibench_backslash" , timemax , "timemax" , 4 , timemin )
    apifun_checkgreq ( "scibench_backslash" , nfact , "nfact" , 4 , 1+%eps )

    function t = backslashtest ( n )
        //
        // Performs the backslash test.
        while ( %t )
            A = rand(n,n,"normal");
            b = rand(n,1,"normal");
            c = ceil(log10(1/rcond(A)));
            if ( c <= 7 ) then
                break
            else
                // Avoid least square computation.
                // See : http://bugzilla.scilab.org/show_bug.cgi?id=7487
                msg = sprintf(gettext("%s: Skipping ill-conditionned matrix: condition=10^%d."),..
                "scibench_backslash",c);
                warning(msg)
            end
        end
        tic()
        x = A\b
        t = toc()
    endfunction
    function stop = backslashoutput ( k , n , t , verbose , dispgraph )
        //
        // Prints the mflops.
        mflops = floor((2/3*n.^3 + 2*n.^2)./t./1.e6);
        if ( dispgraph ) then
          plot(n,mflops,"bo-")
        end
        if ( verbose ) then
          mprintf("Run #%d: n=%6d, T=%.3f (s), Mflops=%6d\n",k,n,t,mflops)
        end
        stop = %f
    endfunction
    //
    // Proceed...
    if ( dispgraph ) then
      xtitle("Backslash","Matrix Order (n)","Megaflops");
    end
    perftable =  scibench_dynbenchfun ( backslashtest , %f , %f , timemin , timemax , nfact , list(backslashoutput,verbose,dispgraph) );
    //
    n=perftable(:,1)
    t=perftable(:,2)
    mflops = floor((2/3*n.^3 + 2*n.^2)./t./1.e6);
    perftable(:,3) = mflops
    // Search for best performance
    [M,k] = max(perftable(:,3));
    if ( verbose ) then
      mprintf("Best performance:")
      mprintf(" N=%d, T=%.3f (s), MFLOPS=%d\n",perftable(k,1),perftable(k,2),perftable(k,3));
    end
endfunction

function argin = argindefault ( rhs , vararglist , ivar , default )
  // Returns the value of the input argument #ivar.
  // If this argument was not provided, or was equal to the
  // empty matrix, returns the default value.
  if ( rhs < ivar ) then
    argin = default
  else
    if ( vararglist(ivar) <> [] ) then
      argin = vararglist(ivar)
    else
      argin = default
    end
  end
endfunction

