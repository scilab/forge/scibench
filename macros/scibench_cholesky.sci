// Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function perftable = scibench_cholesky ( varargin)
    // Benchmarks the Cholesky decomposition.
    //
    // Calling Sequence
    //   perftable = scibench_cholesky ( )
    //   perftable = scibench_cholesky ( verbose )
    //   perftable = scibench_cholesky ( verbose , dispgraph )
    //   perftable = scibench_cholesky ( verbose , dispgraph , timemin )
    //   perftable = scibench_cholesky ( verbose , dispgraph , timemin , timemax )
    //   perftable = scibench_cholesky ( verbose , dispgraph , timemin , timemax , nfact )
    //
    // Parameters
    //   verbose : a 1-by-1 matrix of booleans, set to %t to print messages during and at the end of the experiment (default verbose = %t).
    //   dispgraph : a 1-by-1 matrix of booleans, set to %t to create a plot (default dispgraph = %t).
    //   timemin : a 1-by-1 matrix of doubles, the minimum time (in seconds) to measure the mflops (default timemin = 0.001). Must be positive.
    //   timemax : a 1-by-1 matrix of doubles, the maximum time (in seconds) to measure the mflops (default timemax = 0.1). Must be larger than timemin. The experiment ends when this time is reached.
    //   nfact : a 1-by-1 matrix of doubles, the multiplication factor for the size n of the matrix (default nfact = 1.2). Must be greater than 1. At each step of the algorithm, the size n of the matrix is updated with n = n * nfact.
    //   perftable : a m-by-3 matrix of doubles, the performances.
    //   perftable(k,1) : size of the matrix for experiment #k
    //   perftable(k,2) : wall clock time for experiment #k
    //   perftable(k,3) : number of megaflops achieved for experiment #k
    //
    // Description
    //   This function allows to measure the performance of the Cholesky decomposition of
    //   dense real matrices.
    //
    //   To measure the performance we count the number of megaflops as n^3/3,
    //   where n is the size of the matrix.
    //   The wall clock time is measured with the tic and toc functions.
    //
    //   The algorithm starts with a matrix size equal to n = 1.
    //   Then the matrix size is updated, by multiplying n by nfact.
    //
    // Examples
    // lines(0);
    // stacksize("max");
    // scf();
    // scibench_cholesky ( );
    // // Do not print a message
    // scibench_cholesky ( %f );
    // // Do not create the graph
    // scibench_cholesky ( [] , %f );
    // // Set timemin
    // perftable = scibench_cholesky ( [] , [] , 0.1 );
    // // Set timemax
    // perftable = scibench_cholesky ( [] , [] , 0.1 , 8 );
    // // Set nfact
    // perftable = scibench_cholesky ( [] , [] , 0.1 , 8 , 1.5 );
    //
    // Bibliography
    // "Programming in Scilab", Michael Baudin, 2010, http://forge.scilab.org/index.php/p/docprogscilab/downloads/
    // "Linear algebra performances", http://wiki.scilab.org/Linalg_performances
    // "Benchmarks: LINPACK and MATLAB - Fame and fortune from megaflops", Cleve Moler, 1994
    //
    // Authors
    //   2010 - DIGITEO - Michael Baudin

    [lhs, rhs] = argn()
    apifun_checkrhs ( "scibench_cholesky" , rhs , 0:5 )
    apifun_checklhs ( "scibench_cholesky" , lhs , 0:1 )
    //
    // Get arguments
    verbose = argindefault ( rhs , varargin , 1 , %t )
    dispgraph = argindefault ( rhs , varargin , 2 , %t )
    timemin = argindefault ( rhs , varargin , 3 , 0.001 )
    timemax = argindefault ( rhs , varargin , 4 , 0.1 )
    nfact = argindefault ( rhs , varargin , 5 , 1.2 )
    //
    // Check Type
    apifun_checktype ( "scibench_cholesky" , verbose , "verbose" , 1 , "boolean" )
    apifun_checktype ( "scibench_cholesky" , dispgraph , "dispgraph" , 2 , "boolean" )
    apifun_checktype ( "scibench_cholesky" , timemin , "timemin" , 3 , "constant" )
    apifun_checktype ( "scibench_cholesky" , timemax , "timemax" , 4 , "constant" )
    apifun_checktype ( "scibench_cholesky" , nfact , "nfact" , 5 , "constant" )
    //
    // Check Size
    apifun_checkscalar ( "scibench_cholesky" , verbose , "verbose" , 1 )
    apifun_checkscalar ( "scibench_cholesky" , dispgraph , "dispgraph" , 2 )
    apifun_checkscalar ( "scibench_cholesky" , timemin , "timemin" , 3 )
    apifun_checkscalar ( "scibench_cholesky" , timemax , "timemax" , 4 )
    apifun_checkscalar ( "scibench_cholesky" , nfact , "nfact" , 5 )
    //
    // Check Content
    apifun_checkgreq ( "scibench_cholesky" , timemin , "timemin" , 3 , 0 )
    apifun_checkgreq ( "scibench_cholesky" , timemax , "timemax" , 4 , timemin )
    apifun_checkgreq ( "scibench_cholesky" , nfact , "nfact" , 4 , 1+%eps )

    function A = createsymdiagdom ( n )
        // Create a n-by-n symetric diagonal dominant matrix.
        A = rand(n,n,"normal")
        A = 0.5*(A + A')
        A = A - eye(A).*A
        // Avoid a zero diagonal (e.g. n=1).
        tiny=number_properties("tiny")
        v = sum(abs(A)*1.1+eye(n,n)*tiny,"c")
        D = eye(A).*diag(v)
        A = A + D
        // Check that the diagonal is strictly dominant.
        //diagdom = and(diag(A) - sum(abs(A-eye(A).*A),"c")>0)
    endfunction

    function t = benchcholesky (n)
        // Benchmarks the Cholesky decomposition.
        A = createsymdiagdom ( n )
        tic()
        C = chol ( A )
        t = toc()
    endfunction
    function stop = choleskyoutput (k,n,t,verbose , dispgraph )
        // Prints a message during the Cholesky benchmark.
        mflops = floor(n^3/3/t/1.e6);
        if ( verbose ) then
            mprintf("Run #%3d, n=%5d, t=%.3f (s), Mflops=%6d\n",k,n,t,mflops)
        end
        if ( dispgraph ) then
            plot(n,mflops,"bo-")
        end
        stop = %f
    endfunction
    //
    // Proceed...
    if ( dispgraph ) then
      xtitle("Cholesky decomposition","Matrix Order (n)","Megaflops");
    end
    perftable =  scibench_dynbenchfun ( benchcholesky , %f , %f , timemin , timemax , nfact , list(choleskyoutput,verbose,dispgraph) );
    //
    n=perftable(:,1)
    t=perftable(:,2)
    mflops = floor(n .^ 3 ./ 3 ./ t ./ 1.e6);
    perftable(:,3) = mflops
    // Search for best performance
    [M,k] = max(perftable(:,3));
    if ( verbose ) then
      mprintf("Best performance:")
      mprintf(" N=%d, T=%.3f (s), MFLOPS=%d\n",perftable(k,1),perftable(k,2),perftable(k,3));
    end
endfunction
function argin = argindefault ( rhs , vararglist , ivar , default )
  // Returns the value of the input argument #ivar.
  // If this argument was not provided, or was equal to the
  // empty matrix, returns the default value.
  if ( rhs < ivar ) then
    argin = default
  else
    if ( vararglist(ivar) <> [] ) then
      argin = vararglist(ivar)
    else
      argin = default
    end
  end
endfunction

