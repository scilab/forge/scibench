// Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2010 - Neeraj Sharma and Matthias K. Gobbert

//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function v = scibench_poissonAu ( u )
    // Returns the A*u sparse product for 2D Poisson PDE.
    //
    // Calling Sequence
    //   v = scibench_poissonAu ( u )
    //
    // Parameters
    //   u : a m-by-1 matrix of doubles, the vector.
    //   A : a m-by-m matrix of doubles, the matrix-vector product v=A*u
    //
    // Description
    // We compute the numerical solution with finite
    // differences for the Poisson problem with homogeneous
    // Dirichlet boundary conditions.
    //
    // We consider the 2 dimensional problem Partial Differential Equation:
    //
    // <latex>
    // \begin{array}{rlll}
    // - \Delta u &=& f &\textrm{ in the domain,} \\
    //          u &=& 0 &\textrm{ on the frontier.}
    // \end{array}
    // </latex>
    //
    // where the Laplace operator is
    //
    // <latex>
    // \Delta u = \frac{\partial^2 u}{dx^2} + \frac{\partial^2 u}{dy^2}
    // </latex>
    //
    // We consider the domain 0 <= x1 <= 1, 0 <= x2 <= 1.
    // The function f is defined by
    // f(x, y) = -2pi^2 cos(2pix) sin^2(piy) - 2pi^2 sin^2(pix) cos(2piy).
    // The solution is u(x, y) = sin^2(pix) sin^2(piy).
    // We use a grid of N-by-N points.
    // We use a second order finite difference approximation of the Laplace
    // operator.
    //
    // The matrix A is not stored: only the matrix-vector product v=A*u is
    // performed.
    //
    // Examples
    // stacksize("max");
    // N = 50;
    // m = N^2;
    // u = ones(m,1)
    // v1 = scibench_poissonAu ( u );
    // // Compute explicitely the matrix-vector product:
    // A = scibench_poissonA(50);
    // v2 = A*u;
    // norm(v1-v2)
    //
    // Bibliography
    // "A Comparative Evaluation Of Matlab, Octave, Freemat, And Scilab For Research And Teaching", 2010, Neeraj Sharma and Matthias K. Gobbert

    N = sqrt(size(u,"*"));
    U = matrix(u,[N,N]);
    V = 4*U;
    V(:,2:$)   = V(:,2:$)   - U(:,1:$-1);
    V(2:$,:)   = V(2:$,:)   - U(1:$-1,:);
    V(1:$-1,:) = V(1:$-1,:) - U(2:$,:);
    V(:,1:$-1) = V(:,1:$-1) - U(:,2:$);
    v = V(:)
endfunction
