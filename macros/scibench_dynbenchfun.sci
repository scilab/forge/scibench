// Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function perftable = scibench_dynbenchfun ( varargin )
    // Benchmarks a function with increasing data size.
    //
    // Calling Sequence
    //   perftable = scibench_dynbenchfun ( fun )
    //   perftable = scibench_dynbenchfun ( fun , verbose )
    //   perftable = scibench_dynbenchfun ( fun , verbose , dispgraph )
    //   perftable = scibench_dynbenchfun ( fun , verbose , dispgraph , timemin )
    //   perftable = scibench_dynbenchfun ( fun , verbose , dispgraph , timemin , timemax )
    //   perftable = scibench_dynbenchfun ( fun , verbose , dispgraph , timemin , timemax , nfact )
    //   perftable = scibench_dynbenchfun ( fun , verbose , dispgraph , timemin , timemax , nfact , outfun )
    //
    // Parameters
    //   fun : a function or a list, the function to be executed. If fun is a list, the first element is expected to be a function and the remaining elements of the list are input arguments of the function, which are appended at the end.
    //   verbose : a 1-by-1 matrix of booleans, set to %t to print messages during and at the end of the experiment (default verbose=%t).
    //   dispgraph : a 1-by-1 matrix of booleans, set to %t to create a plot (default dispgraph=%t).
    //   timemin : a 1-by-1 matrix of doubles, the minimum time (in seconds) to measure the mflops (default timemin=0.001). Must be positive.
    //   timemax : a 1-by-1 matrix of doubles, the maximum time (in seconds) to measure the mflops (default timemax=0.1). Must be larger than timemin. The experiment ends when this time is reached.
    //   nfact : a 1-by-1 matrix of doubles, the multiplication factor for the size n of the matrix (default nfact=1.2). Must be greater than 1. At each step of the algorithm, the size n of the matrix is updated with n = n * nfact.
    //   outfun : a function or a list, the output function (default is an empty output function). If outfun is a list, the first element is expected to be a function and the remaining elements of the list are input arguments of the function, which are appended at the end.
    //   perftable : a m-by-3 matrix of doubles, the performances.
    //   perftable(k,1) : size of the data for experiment #k
    //   perftable(k,2) : wall clock time for experiment #k
    //
    // Description
    //   This function allows to measure the performance of a function depending on an increasing
    //   parameter representing the size of the problem.
    //
    //   The wall clock time is measured with the tic and toc functions.
    //
    //   The algorithm starts with a data size size equal to n = 1.
    //   Then the data size is updated, by using n = n * nfact.
    //   The performance is measured as soon as the elapsed time is greater than timemin.
    //   The algorithm stops when the elapsed time is greater than timemax.
    //
    // Any argument equal to the empty matrix [] is replaced by its default value.
    //
    //   The header of the function fun must be
    //   <programlisting>
    //     t=fun(n)
    //   </programlisting>
    //   where n represents the current data size, and
    //   t measures the time to perform the task (in seconds).
    //
    // It might happen that the function requires additionnal arguments to be evaluated.
    // In this case, we can use the following feature.
    // The function fun can also be the list (f,a1,a2,...).
    // In this case f, the first element in the list, must have the header:
    //   <programlisting>
    //     t = f ( x , a1 , a2 , ... )
    //   </programlisting>
    // where the input arguments a1, a2, ...
    // will be automatically be appended at the end of the calling sequence.
    //
    // The output function outfun should have header
    //   <programlisting>
    //     stop = outfun ( k , n , t )
    //   </programlisting>
    // where k is the simulation index, n is the size of the problem and t is the time required to perform the run.
    // Here, stop is a boolean, which is %t if the algorithm must stop.
    // This feature may be used in the case where the algorithm never stops, because
    // the time never reaches the upper limit.
    // The user may then configure a maximum number of runs, or whatever appropriate.
    //
    // It might happen that the output function requires additionnal arguments to be evaluated.
    // In this case, we can use the following feature.
    // The output function outfun can also be a list, with header
    //   <programlisting>
    //     stop = outfun ( k , n , t , a1 , a2 , ... )
    //   </programlisting>
    // In this case, the outfun variable should hold the list (f,a1,a2,...) and the input arguments a1, a2, ...
    // will be automatically be appended at the end of the calling sequence.
    //
    // In practice, the function to benchmark may create temporary variables before
    // actually perform the computation.
    // This pre-processing has some CPU cost which may change the measured
    // performance significantly.
    //
    // Examples
    // lines(0);
    // stacksize("max");
    // function t = pascalup_col (n)
    //   // Pascal up matrix.
    //   // Column by column version
    //   tic()
    //   c = eye(n,n)
    //   c(1,:) = ones(1,n)
    //   for i = 2:(n-1)
    //     c(2:i,i+1) = c(1:(i-1),i)+c(2:i,i)
    //   end
    //   t = toc()
    // endfunction
    // scf();
    // perftable = scibench_dynbenchfun ( pascalup_col )
    // perftable = scibench_dynbenchfun ( pascalup_col , %t );
    // perftable = scibench_dynbenchfun ( pascalup_col , %t , %t );
    // perftable = scibench_dynbenchfun ( pascalup_col , %t , %t , 0.1 );
    // perftable = scibench_dynbenchfun ( pascalup_col , %t , %t , 0.1 , 1 );
    // perftable = scibench_dynbenchfun ( pascalup_col , %t , %t , 0.1 , 1 , 1.2 );
    //
    // // Do not print a message
    // scibench_dynbenchfun ( pascalup_col , %f );
    //
    // // Do not create the graph
    // scibench_dynbenchfun ( pascalup_col , [] , %f );
    //
    // // With an additionnal argument
    // function t = pascalup_col2 ( n , alpha )
    //    tic()
    //    c = eye(n,n) * alpha
    //    c(1,:) = ones(1,n)
    //    for i = 2:(n-1)
    //       c(2:i,i+1) = c(1:(i-1),i)+c(2:i,i)
    //    end
    //    t = toc()
    // endfunction
    // scibench_dynbenchfun ( list(pascalup_col2,2) );
    //
    // //
    // // Use a callback to print the timings.
    // function stop = myoutfun(k,n,t)
    //     mprintf("Run #%d, n=%d, t=%.2f\n",k,n,t)
    //     stop = %f
    // endfunction
    // scibench_dynbenchfun ( pascalup_col , %f , %f , [] , [] , [] , myoutfun );
    // //
    // // Use a callback to print the timings.
    // // The callback requires additionnal arguments.
    // function stop = myoutfun2(k,n,t,data)
    //     mprintf("Run #%d, n=%d, t=%.2f\n",k,n,t)
    //     stop = %f
    // endfunction
    // scibench_dynbenchfun ( pascalup_col , %f , %f , [] , [] , [] , list(myoutfun2,2) );
    //
    // // Use a callback to print the timings and stop the algorithm.
    // function stop = myoutfun4(k,n,t,kmax)
    //   mprintf("Run #%d, n=%d, t=%.2f\n",k,n,t)
    //   stop = ( k >= kmax )
    // endfunction
    // scibench_dynbenchfun ( pascalup_col , %f , %f , [] , [] , [] , list(myoutfun4,2) );
    //
    // Authors
    //   2010 - DIGITEO - Michael Baudin

    [lhs, rhs] = argn()
    apifun_checkrhs ( "scibench_dynbenchfun" , rhs , 1:7 )
    apifun_checklhs ( "scibench_dynbenchfun" , lhs , 0:1 )
    //
    // Get arguments
    __benchfun_fun__ = varargin(1)
    verbose = argindefault ( rhs , varargin , 2 , %t )
    dispgraph = argindefault ( rhs , varargin , 3 , %t )
    timemin = argindefault ( rhs , varargin , 4 , 0.001 )
    timemax = argindefault ( rhs , varargin , 5 , 0.1 )
    nfact = argindefault ( rhs , varargin , 6 , 1.2 )
    __dynbenchfun_outfun__ = argindefault ( rhs , varargin , 7 , [] )
    //
    // Check Type
    apifun_checktype ( "scibench_dynbenchfun" , __benchfun_fun__ , "fun" , 1 , ["function" "list" "fptr"] )
    if ( typeof(__benchfun_fun__) == "list" ) then
        apifun_checktype ( "scibench_dynbenchfun" , __benchfun_fun__(1) , "fun(1)" , 1 , ["function" "fptr"] )
    end
    apifun_checktype ( "scibench_dynbenchfun" , verbose , "verbose" , 2 , "boolean" )
    apifun_checktype ( "scibench_dynbenchfun" , dispgraph , "dispgraph" , 3 , "boolean" )
    apifun_checktype ( "scibench_dynbenchfun" , timemin , "timemin" , 4 , "constant" )
    apifun_checktype ( "scibench_dynbenchfun" , timemax , "timemax" , 5 , "constant" )
    apifun_checktype ( "scibench_dynbenchfun" , nfact , "nfact" , 6 , "constant" )
    if ( __dynbenchfun_outfun__ <> [] ) then
      apifun_checktype ( "scibench_dynbenchfun" , __dynbenchfun_outfun__ , "outfun" , 7 , ["function" "list" "fptr"] )
      if ( typeof(__dynbenchfun_outfun__) == "list" ) then
        apifun_checktype ( "scibench_dynbenchfun" , __dynbenchfun_outfun__(1) , "outfun(1)" , 7 , "function" )
      end
    end
    //
    // Check Size
    apifun_checkscalar ( "scibench_dynbenchfun" , verbose , "verbose" , 2 )
    apifun_checkscalar ( "scibench_dynbenchfun" , dispgraph , "dispgraph" , 3 )
    apifun_checkscalar ( "scibench_dynbenchfun" , timemin , "timemin" , 4 )
    apifun_checkscalar ( "scibench_dynbenchfun" , timemax , "timemax" , 5 )
    apifun_checkscalar ( "scibench_dynbenchfun" , nfact , "nfact" , 6 )
    //
    // Check Content
    apifun_checkgreq ( "scibench_dynbenchfun" , timemin , "timemin" , 4 , 0 )
    apifun_checkgreq ( "scibench_dynbenchfun" , timemax , "timemax" , 5 , timemin )
    apifun_checkgreq ( "scibench_dynbenchfun" , nfact , "nfact" , 6 , 1+%eps )

    // Compute tyep of measured function
    typfun = typeof(__benchfun_fun__)
    if ( or ( typfun == "list" ) ) then
        __benchfun_fun__f = __benchfun_fun__(1)
    end
    //
    // Make a loop over n
    if ( dispgraph ) then
        xtitle("","Data size (n)","Time (s)");
    end
    n = 1;
    k = 1;
    perftable = [];
    if ( __dynbenchfun_outfun__ <> [] ) then
        cbktype = typeof(__dynbenchfun_outfun__)
        if ( cbktype == "list" ) then
            // List or tlist
            __dynbenchfun_outfun__f = __dynbenchfun_outfun__(1);
        end
    end
    while ( %t )
        if ( or ( typfun == ["function" "fptr"] ) ) then
            t = __benchfun_fun__(n)
        else
            t = __benchfun_fun__f(n,__benchfun_fun__(2:$))
        end
        //
        // Checkout output t
        if ( typeof(t) <> "constant" ) then
            error(sprintf(gettext("%s: The output argument t of measured function must be a real matrix of doubles.\n"), ..
                "scibench_dynbenchfun"))
        end
        if ( size(t,"*") <> 1 ) then
            error(sprintf(gettext("%s: The output argument t of measured function must be a 1-by-1 real matrix of doubles.\n"), ..
                "scibench_dynbenchfun"))
        end
        if ( t < 0 ) then
            error(sprintf(gettext("%s: The output argument t of measured function must be positive.\n"), ..
                "scibench_dynbenchfun"))
        end
        stop = %f
        if ( t > timemin ) then
            perftable(k,:) = [n t];
            if ( dispgraph ) then
                plot(n,t,"bo-")
            end
            if ( verbose ) then
                mprintf("Run #%d: n=%6d, T=%.3f (s)\n",k,perftable(k,1),perftable(k,2))
            end
            if ( __dynbenchfun_outfun__ <> [] ) then
                cbktype = typeof(__dynbenchfun_outfun__)
                if ( cbktype == "list" ) then
                    // List or tlist
                    stop = __dynbenchfun_outfun__f ( k , n , t , __dynbenchfun_outfun__(2:$));
                elseif ( or(cbktype == ["function" "fptr"] ) ) then
                    // Macro or compiled macro
                    stop = __dynbenchfun_outfun__ ( k , n , t );
                end
            end
            k = k+1;
        end
        if ( stop | t > timemax ) then
            break
        end
        n = ceil(nfact * n);
    end
endfunction

function argin = argindefault ( rhs , vararglist , ivar , default )
  // Returns the value of the input argument #ivar.
  // If this argument was not provided, or was equal to the
  // empty matrix, returns the default value.
  if ( rhs < ivar ) then
    argin = default
  else
    if ( vararglist(ivar) <> [] ) then
      argin = vararglist(ivar)
    else
      argin = default
    end
  end
endfunction

