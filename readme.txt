Scilab Benchmark toolbox

Purpose
---------

The goal of this project is to provide a tools to measure the performances of a function.
Moreover, the module provides a collection of benchmark scripts to measure the performance of Scilab.

Features
--------

This module provides the following functions.

 * scibench_backslash — Benchmarks the backslash operator.
 * scibench_benchfun — Benchmarks a function and measure its performance.
 * scibench_cholesky — Benchmarks the Cholesky decomposition.
 * scibench_dynbenchfun — Benchmarks a function with increasing data size.
 * scibench_getsettings — Inquire the parameters of Scilab and its environment.
 * scibench_latticeboltz — Performs a 2D Lattice Boltzmann simulation with Scilab.
 * scibench_matmul — Benchmarks the matrix-matrix multiplication.
 * scibench_poisson — Solves the 2D Poisson PDE.
 * scibench_poissonA — Returns a sparse matrix for 2D Poisson PDE.
 * scibench_poissonAu — Returns the A*u sparse product for 2D Poisson PDE.

Type "help scibench_overview" for a quick start.

Dependencies
------------

 * This module depends on the apifun module.
 * This module depends on the assert module.
 * This module depends on the helptbx module.

Forge
-----

http://forge.scilab.org/index.php/p/scibench/

ATOMS
-----

http://atoms.scilab.org/toolboxes/scibench

TODO
----

 * In dynbenchfun, change the header of the measured function from
 [o_1,o_2,...,o_nlhs]=fun(n) to t=fun(n), so that the function can measure the
 part of the algorithm which is to be evaluated.
 This will allow to separate the preparation of the data from the actual
 routine to compute.
 See : http://forge.scilab.org/index.php/p/scibench/issues/196/
 * Add benchmark from : http://www.sciviews.org/benchmark/

Authors
-------

 * 2010 - 2011 - DIGITEO - Michael Baudin
 * 2010 - DIGITEO - Vincent Lejeune
 * 2006-2008 - Jonas Latt
 * Adriano Sciacovelli

Licence
-------

This toolbox is distributed under the CeCILL license :
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

Acknowledgements
----------------

Michael Baudin thanks Clément David and Allan Cornet.

