// Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
// Do not check ref, because timings cannot be reproduced exactly.
// <-- NO CHECK REF -->
//

v = getversion("scilab")
if v(1) < 6 then
    stacksize("max");
end
function u = mysolverBackslash(N,b)
    A = scibench_poissonA(N);
    u = A\b;
endfunction
scf();
[timesec,enorminf,h] = scibench_poisson(10, %t , %t , mysolverBackslash );

//
// With PCG
function u = mysolverPCG(N,b)
    tol = 0.000001;
    maxit = 9999;
    u = zeros(N^2,1);
    if exists("pcg") then
        [u, flag, iter, res] = pcg(scibench_poissonAu,b,tol,maxit,[],[],u);
    else
        // workaround function rename in Scilab 5.5.2
        [u, flag, iter, res] ..
        = conjgrad(scibench_poissonAu, b, "pcg", tol, maxit, [], [], u);
    end
endfunction
[timesec,enorminf,h] = scibench_poisson(10, %f , %t , mysolverPCG );
