// Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
// Do not check ref, because timings cannot be reproduced exactly.
// <-- NO CHECK REF -->
//

stacksize("max");
lines(0);
perftable = scibench_backslash ( );
perftable = scibench_backslash ( %f );
perftable = scibench_backslash ( %f , %f );
perftable = scibench_backslash ( %f , %f , 0.01 );
perftable = scibench_backslash ( %f , %f , [] , 0.1 );
perftable = scibench_backslash ( %f , %f , [] , 0.1 , 1.5 );

