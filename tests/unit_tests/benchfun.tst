// Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
// Do not check ref, because timings cannot be reproduced exactly.
// <-- NO CHECK REF -->
//

/////////////////////////////////////////////////////////////
// Pascal up matrix.
// Column by column version
function t = pascalup_col0rhs ()
   tic()
   n = 100
   c = eye(n,n)
   c(1,:) = ones(1,n)
   for i = 2:(n-1)
      c(2:i,i+1) = c(1:(i-1),i)+c(2:i,i)
   end
   t = toc()
endfunction

//
[t,msg] = scibench_benchfun ( pascalup_col0rhs );
assert_checkequal ( size(t) , [10 1] );
assert_checkequal ( typeof(t) , "constant" );
assert_checkequal ( and(t>=0) , %t );
assert_checkequal ( size(msg) , [1 1] );
assert_checkequal ( typeof(msg) , "string" );
//
[t,msg] = scibench_benchfun ( pascalup_col0rhs , "pascalup_col" );
assert_checkequal ( size(t) , [10 1] );
assert_checkequal ( typeof(t) , "constant" );
assert_checkequal ( and(t>=0) , %t );
assert_checkequal ( size(msg) , [1 1] );
assert_checkequal ( typeof(msg) , "string" );
//
// With an extra parameter
function t = pascalup_col (n)
   [lhs, rhs] = argn()
   if ( rhs <> 1 ) then
     error("Wrong number of input arguments")
   end
   tic()
   c = eye(n,n)
   c(1,:) = ones(1,n)
   for i = 2:(n-1)
      c(2:i,i+1) = c(1:(i-1),i)+c(2:i,i)
   end
   t = toc()
endfunction

[t,msg] = scibench_benchfun ( list(pascalup_col,100) , "pascalup_col" );
assert_checkequal ( size(t) , [10 1] );
assert_checkequal ( typeof(t) , "constant" );
assert_checkequal ( and(t>=0) , %t );
assert_checkequal ( size(msg) , [1 1] );
assert_checkequal ( typeof(msg) , "string" );
//
[t,msg] = scibench_benchfun ( list(pascalup_col,100) , "pascalup_col" , 10 );
assert_checkequal ( size(t) , [10 1] );
assert_checkequal ( typeof(t) , "constant" );
assert_checkequal ( and(t>=0) , %t );
assert_checkequal ( size(msg) , [1 1] );
assert_checkequal ( typeof(msg) , "string" );
//
[t,msg] = scibench_benchfun ( list(pascalup_col,100) , "pascalup_col" , 10 , %t );
assert_checkequal ( size(t) , [10 1] );
assert_checkequal ( typeof(t) , "constant" );
assert_checkequal ( and(t>=0) , %t );
assert_checkequal ( size(msg) , [1 1] );
assert_checkequal ( typeof(msg) , "string" );

// Disable the message
clear t
clear msg
[t,msg] = scibench_benchfun ( list(pascalup_col,100) , "pascalup_col" , 10 , %f );


///////////////////////////////////////////////////////////////
//
// Another case
function t = subsets ( E , m )
   // Authors : Chancelier, Pincon
   // compute all the subsets with m elements of the set E
   // (E must be given as a row vector and all its elements
   // must be differents). For sets of numbers.
   [lhs, rhs] = argn()
   if ( rhs <> 2 ) then
     error("Wrong number of input arguments")
   end
   tic()
   n = length(E)
   if m > n then
      F = []
   elseif m == n
      F = E
   elseif m == 1
      F = E';
   else
      F = [];
      for i = 1:n-m+1
         EE = E(i+1:n)
         FF = subsets(EE,m-1)
         mm = size(FF,1)
         F = [F ; E(i)*ones(mm,1),FF]
      end
   end
   t = toc()
endfunction
scibench_benchfun ( list(subsets,1:10,3) , "subsets" , 10 , %t );
//
// With a primitive
function t = mysin(n1,n2)
  tic()
  y = sin(ones(n1,n2))
  t = toc()
endfunction
scibench_benchfun ( list(mysin,100,100) , "sin" , 10 , %t );
//
// Use a callback to print the timings
function stop = myoutfun(k,t)
  global __counter__
  // Do nothing
  __counter__ = __counter__ + 1
  stop = %f
  //mprintf("Run #%d, t=%.2f\n",k,t)
endfunction
global __counter__
__counter__ = 0;
scibench_benchfun ( list(pascalup_col,100) , "pascalup_col" , [] , [] , myoutfun );
assert_checkequal ( __counter__>0 , %t );
//
// Use a callback to print the timings.
// The callback requires additionnal arguments.
function stop = myoutfun2(k,t,data)
  global __counter__
  // Do nothing
  __counter__ = __counter__ + 1
  stop = %f
  if ( data <> 2 ) then
    error ( "Unknown data" )
  end
  //mprintf("Run #%d, t=%.2f\n",k,t)
endfunction
global __counter__
__counter__ = 0;
scibench_benchfun ( list(pascalup_col,100) , "pascalup_col" , [] , [] , list(myoutfun2,2) );
assert_checkequal ( __counter__>0 , %t );

//
// Use a callback to print the timings.
// The callback stops the algorithm
function stop = myoutfun3(k,t)
  global __counter__
  // Do nothing
  __counter__ = __counter__ + 1
  stop = ( __counter__ >= 2 )
  //mprintf("Run #%d, t=%.2f\n",k,t)
endfunction
global __counter__
__counter__ = 0;
scibench_benchfun ( list(pascalup_col,100) , "pascalup_col" , [] , [] , myoutfun3 );
assert_checkequal ( __counter__ , 2 );

//
// Use a callback to print the timings and stop the algorithm.
function stop = myoutfun4(k,t,kmax)
  global __counter__
  // Do nothing
  __counter__ = __counter__ + 1
  stop = ( k >= kmax )
  //mprintf("Run #%d, t=%.2f\n",k,t)
endfunction
global __counter__
__counter__ = 0;
scibench_benchfun ( list(pascalup_col,100) , "pascalup_col" , [] , [] , list(myoutfun4,2) );
assert_checkequal ( __counter__ , 2 );

