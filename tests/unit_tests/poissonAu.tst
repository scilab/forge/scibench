// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
// Do not check ref, because timings cannot be reproduced exactly.
// <-- NO CHECK REF -->
//

stacksize("max");
N = 50;
m = N^2;
u = ones(m,1);
v1 = scibench_poissonAu ( u );
// Compute explicitely the matrix-vector product:
A = scibench_poissonA(50);
v2 = A*u;
assert_checkequal(v1,v2);
