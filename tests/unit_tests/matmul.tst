// Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
// Do not check ref, because timings cannot be reproduced exactly.
// <-- NO CHECK REF -->
//

stacksize("max");
lines(0);
// Do not print a message
scibench_matmul ( %f );
// Do not create the graph
scibench_matmul ( %t , %f );
scibench_matmul ( %f , %f , 0.001 );
scibench_matmul ( %f , %f , 0.001 , 0.05 );
//
perftable = scibench_matmul ( %t , %t , 0.001 , 0.05 , 1.1 );
assert_checkequal ( size(perftable,"c") , 3 );
assert_checkequal ( typeof(perftable) , "constant" );



