// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

stacksize("max");
//
// Sparse backslash
function u=mysolverBackslash(N, b)
    A = scibench_poissonA(N);
    u = A\b;
endfunction

//
// PCG solver:
function u=mysolverPCG(N, b)
    tol = 0.000001;
    maxit = 9999;
    u = zeros(N^2,1);
    [u,flag,iter,res] = pcg(scibench_poissonAu,b,tol,maxit,[],[],u);
endfunction
//
// With UMFpack
function u = mysolverUMF(N,b)
    A = scibench_poissonA(N);
    humf = umf_lufact(A);
    u = umf_lusolve(humf,b)
    umf_ludel(humf)
endfunction
//
// With TAUCS
// See bug http://bugzilla.scilab.org/show_bug.cgi?id=8824
// This test makes Scilab unstable.
function u = mysolverTAUCS(N,b)
    A = scibench_poissonA(N);
    hchol = taucs_chfact(A);
    u = taucs_chsolve(hchol,b)
    taucs_chdel(hchol)
endfunction
//
// Dynamic benchmark
function t = poissonBenchmark(N,solver);
    [timesec,enorminf,h] = scibench_poisson(N, %f , %f , solver );
    t = timesec
endfunction
verbose = %t;
dispgraph = %f;
timemin= 0.001;
timemax = 0.2;
mprintf("With sparse backslash...\n");
perftableBack = scibench_dynbenchfun ( list(poissonBenchmark,mysolverBackslash) , verbose , dispgraph , timemin , timemax );
mprintf("With PCG...\n");
perftablePCG = scibench_dynbenchfun ( list(poissonBenchmark,mysolverPCG) , verbose , dispgraph , timemin , timemax );
mprintf("With UMF...\n");
perftableUMF = scibench_dynbenchfun ( list(poissonBenchmark,mysolverUMF) , verbose , dispgraph , timemin , timemax );
//
scf();
plot(perftableBack(:,1).^2,perftableBack(:,2),"bo-");
plot(perftablePCG(:,1).^2,perftablePCG(:,2),"ro-");
plot(perftableUMF(:,1).^2,perftableUMF(:,2),"go-");
legend(["Backslash","PCG","UMF"]);
xtitle("Solving 2D Poisson equation with Sparse solvers","Number of equations","Time (s)");


