// ====================================================================
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// This file is released into the public domain
// ====================================================================
demopath = get_absolute_file_path("scibench.dem.gateway.sce");
subdemolist = [
"matmul_SMPaffinity", "matmul_SMPaffinity.sce"; ..
"matmul_MKLthreads", "matmul_MKLthreads.sce"; ..
"matmul_histo", "matmul_histo.sce"; ..
"poisson", "poisson.sce"; ..
];
subdemolist(:,2) = demopath + subdemolist(:,2)
