% Copyright (C) 2010 - DIGITEO - Michael Baudin
%
% This file must be used under the terms of the CeCILL.
% This source file is licensed as described in the file COPYING, which
% you should have received as part of this distribution.  The terms
% are also available at
% http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

% Benchmarking the matrix-matrix product

% References
% "Programming in Scilab", Michael Baudin, 2010, http://forge.scilab.org/index.php/p/docprogscilab/downloads/

%
% A basic benchmark
if ( %f ) then
n = 1000;
A = randn(n,n);
B = randn(n,n);
tic();
C = A * B;
t = toc();
mflops = 2*n^3/t/1.e6;
disp([n t mflops])
end

%
% Let the size of the matrix grow dynamically

timemin = 0.1;
timemax = 8.0;
nfact = 1.2;

%
% Make a loop over n
title('Matrix-Matrix multiply');
xlabel('Matrix Order (n)');
ylabel('Megaflops');
n = 1;
k = 1;
perftable = [];
while ( 1 )
  A = randn(n,n);
  B = randn(n,n);
  tic();
  C = A * B;
  t = toc();
  if ( t > timemin )
    mflops = floor(2*n^3/t/1.e6);
    perftable(k,:) = [n t mflops];
    hold all
    plot(perftable(:,1),perftable(:,3),'bo-')
    hold off
    drawnow
    fprintf(1,'Run #%d: n=%6d, T=%.3f (s), Mflops=%6d\n',k,perftable(k,1),perftable(k,2),perftable(k,3))
    k = k+1;
  end
  if ( t > timemax )
    break
  end
  n = ceil(nfact * n);
end
% Search for best performance
[M,k] = max(perftable(:,3));
fprintf(1,'Best performance:');
fprintf(1,' N=%d, T=%.3f (s), MFLOPS=%d\n',perftable(k,1),perftable(k,2),perftable(k,3));


